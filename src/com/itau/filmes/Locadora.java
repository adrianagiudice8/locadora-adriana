package com.itau.filmes;

public class Locadora {
	//public Filme[] = filmes;
	
	public Filme[] obterFilmes() {
		Filme[] filmes = new Filme [1];
		Filme filme1 = new Filme();
		filme1.titulo = "forrest Gump";
		filme1.anoLancamento = 1994;
		filme1.atores = new Ator[2];
		
		Ator ator1 = new Ator();
		ator1.nome = "Tom";
		ator1.sobrenome = "Hanks";
		
		Ator ator2 = new Ator();
		ator2.nome = "Robin";
		ator2.sobrenome = "Wright";
		
		filme1.atores[0] = ator1;
		filme1.atores[1] = ator2;
		
		filmes[0] = filme1;
	}
 }
